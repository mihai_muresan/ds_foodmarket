package com.muresan.foodmarket.repositories;

import com.muresan.foodmarket.entities.OrderItem;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {
}
