package com.muresan.foodmarket.repositories;

import com.muresan.foodmarket.entities.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
