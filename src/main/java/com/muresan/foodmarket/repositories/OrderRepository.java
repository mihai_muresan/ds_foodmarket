package com.muresan.foodmarket.repositories;

import com.muresan.foodmarket.entities.ClientOrder;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<ClientOrder, Integer> {
}
