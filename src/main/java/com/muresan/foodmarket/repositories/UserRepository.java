package com.muresan.foodmarket.repositories;

import com.muresan.foodmarket.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsernameAndPassword(String username, String password);

    User findByUsername(String username);
}
