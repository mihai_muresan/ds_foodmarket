package com.muresan.foodmarket.controllers;

import com.muresan.foodmarket.entities.ClientOrder;
import com.muresan.foodmarket.entities.User;
import com.muresan.foodmarket.repositories.OrderItemRepository;
import com.muresan.foodmarket.repositories.OrderRepository;
import com.muresan.foodmarket.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;


@RestController
public class OrderController {

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final UserRepository userRepository;

    @Autowired
    public OrderController(OrderRepository orderRepository, OrderItemRepository orderItemRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/api/order", method = RequestMethod.POST)
    public void createOrder(@RequestBody ClientOrder clientOrder, HttpSession session) {
        clientOrder.getOrderItems().forEach(this.orderItemRepository::save);

        User sessionUser = (User) session.getAttribute("user");

        User foundUser = this.userRepository.findOne(sessionUser.getId());
        clientOrder.setUser(foundUser);

        this.orderRepository.save(clientOrder);
    }

    @RequestMapping(value = "/api/order", method = RequestMethod.GET)
    public Iterable<ClientOrder> getAll() {
        return this.orderRepository.findAll();
    }
}
