package com.muresan.foodmarket.controllers;

import com.muresan.foodmarket.entities.User;
import com.muresan.foodmarket.repositories.UserRepository;
import javassist.tools.web.BadHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class UserController {
    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/api/user", method = RequestMethod.GET)
    public User user(@RequestParam String username, @RequestParam String password, HttpServletRequest request, HttpSession session) throws BadHttpRequest {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        User foundUser = this.userRepository.findByUsername(username);

        if (foundUser == null || !passwordEncoder.matches(password, foundUser.getPassword())) {
            throw new BadHttpRequest();
        }

        session.invalidate();
        HttpSession newSession = request.getSession();
        newSession.setAttribute("user", foundUser);

        return foundUser;
    }

    @RequestMapping(value = "/api/user", method = RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return this.userRepository.save(user);
    }
}
