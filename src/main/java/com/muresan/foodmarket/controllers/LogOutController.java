package com.muresan.foodmarket.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class LogOutController {
    @RequestMapping("/api/logout")
    public void logOut(HttpServletRequest request, HttpSession session) {

        session.invalidate();
        request.getSession();
    }
}
