package com.muresan.foodmarket.controllers;

import com.muresan.foodmarket.entities.Product;
import com.muresan.foodmarket.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
public class ProductsController {

    private final ProductRepository productRepository;

    @Autowired
    public ProductsController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @RequestMapping(value = "/api/product", method = RequestMethod.GET)
    public Iterable<Product> getAll() {
        return this.productRepository.findAll();
    }

    @RequestMapping(value = "/api/product/{productId}", method = RequestMethod.GET)
    public Product get(@PathVariable("productId") int productId) {
        return this.productRepository.findOne(productId);
    }

    @RequestMapping(value = "/api/product/{productId}", method = RequestMethod.PUT)
    public Product update(@RequestBody Product product) {
        return this.productRepository.save(product);
    }

    @RequestMapping(value = "/api/product", method = RequestMethod.POST)
    @ResponseBody
    public Product create(@Valid @RequestBody Product product) {
        return this.productRepository.save(product);
    }

    @RequestMapping(value = "/api/product/{productId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("productId") int productId) {
        this.productRepository.delete(productId);
    }
}
