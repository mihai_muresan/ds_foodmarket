foodMarket.service('appStates', function () {
    this.login = {
        name: 'main.login',
        url: '/login'
    };

    this.register = {
        name: 'main.register',
        url: '/register'
    };

    this.home = {
        name: 'main.home',
        url: '/home'
    };

    this.cart = {
        name: 'main.cart',
        url: '/cart'
    };

    this.orderSent = {
        name: 'main.orderSent',
        url: '/order-sent'
    };

    this.admin = {
        home: {
            name: 'main.adminHome',
            url: '/admin/home'
        },
        createProduct: {
            name: 'main.adminCreateProduct',
            url: '/admin/product/{productId}'
        },
        listProducts: {
            name: 'main.adminListProducts',
            url: '/admin/products'
        },
        listOrders: {
            name: 'main.adminListOrders',
            url: '/admin/orders'
        }
    }
});