foodMarket.service('apiUrls', [function () {
    this.baseUrl = '/api';

    this.user = this.baseUrl + '/user';

    this.logout = this.baseUrl + '/logout';

    this.admin = {
        order: this.baseUrl + '/order/:orderId',
        product: this.baseUrl + '/product/:productId'
    }
}]);