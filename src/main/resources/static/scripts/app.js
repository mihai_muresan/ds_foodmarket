var foodMarket = angular.module('food-market', [
    'ui.router',
    'ngResource',
    'ngMessages'
]);

foodMarket.run(['$rootScope', 'appStates', 'authService', '$state', function ($rootScope, appStates, authService, $state) {
    $rootScope.appStates = appStates;
    $rootScope.auth = authService;

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
        if (toState.data && toState.data.role !== authService.getCurrentUser().role) {
            event.preventDefault();
            $state.go(appStates.login.name);
        }
    });
}]);

foodMarket.config(['$httpProvider', function ($httpProvider) {
}]);