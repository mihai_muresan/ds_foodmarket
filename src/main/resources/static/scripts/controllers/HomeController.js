foodMarket.controller('HomeController', ['$scope', 'productsService', 'cartService',
    function ($scope, productsService, cartService) {

        $scope.products = [];

        $scope.addToCart = function (product) {
            cartService.add(product);
        };

        $scope.removeFromCart = function (product) {
            cartService.remove(product);
        };

        (function init() {
            productsService.query(function (products) {
                var cartItems = cartService.getAll();

                for (var i = 0; i < cartItems.length; i++) {
                    for (var j = 0; j < products.length; j++) {
                        if (cartItems[i].product.id === products[j].id) {
                            products[j].isInCart = true;
                        }
                    }
                }

                $scope.products = products;
            });
        })();
    }]);
