foodMarket.controller('AdminOrdersController', ['$scope', 'ordersService',
    function ($scope, ordersService) {
        'use strict';

        $scope.products = [];

        (function init() {
            ordersService.query(function (orders) {

                angular.forEach(orders, function (order) {
                    order.totalPrice = 0;
                    order.nrOfItems = 0;

                    angular.forEach(order.orderItems, function (orderItem) {
                        order.nrOfItems += orderItem.quantity;
                        order.totalPrice += orderItem.quantity * orderItem.product.price;
                    });
                });

                $scope.orders = orders;
            })
        })();
    }]);