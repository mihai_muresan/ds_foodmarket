foodMarket.controller('CartController', ['$scope', 'ordersService', 'cartService', '$state', 'appStates',
    function ($scope, ordersService, cartService, $state, appStates) {

        $scope.cartItems = [];

        $scope.sendOrder = function () {
            ordersService.save({"orderItems": $scope.cartItems}, function () {
                $state.go(appStates.orderSent.name);
            });
        };

        $scope.removeCartItem = function (cartItem) {
            cartService.remove(cartItem.product);
            $scope.cartItems = cartService.getAll();
        };

        (function init() {
            $scope.cartItems = cartService.getAll();
        })();
    }]);