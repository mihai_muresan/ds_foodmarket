foodMarket.controller('AdminProductsController', ['$scope', 'productsService',
    function ($scope, productsService) {
        'use strict';

        $scope.products = [];

        $scope.onDeleteProductBtnClicked = function (product) {
            productsService.remove({productId: product.id});

            for (var i = 0; i < $scope.products.length; i++) {
                if ($scope.products[i].id === product.id) {
                    $scope.products.splice(i, 1);
                    break;
                }
            }
        };

        (function init() {
            productsService.query(function (products) {
                $scope.products = products;
            })
        })();
    }]);