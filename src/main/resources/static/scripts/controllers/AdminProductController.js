foodMarket.controller('AdminProductController',
    ['$scope', '$stateParams', 'productsService', '$state', 'appStates',
        function ($scope, $stateParams, productsService, $state, appStates) {
            'use strict';

            $scope.productData = {};

            function onPersistsSuccessful() {
                $state.go(appStates.admin.listProducts.name);
            }

            $scope.onCreateProductFormSubmit = function () {
                if ($scope.productData.id) {
                    productsService.update($scope.productData, onPersistsSuccessful);
                } else {
                    productsService.save($scope.productData, onPersistsSuccessful);
                }
            };

            (function init() {
                var productId = $stateParams.productId;

                if (productId) {
                    productsService.get({productId: productId}, function (product) {
                        $scope.productData = product;
                    });
                }
            })();
        }]);