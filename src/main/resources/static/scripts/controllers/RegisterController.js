foodMarket.controller('RegisterController', ['$scope', 'authService', '$state', 'appStates',
    function ($scope, authService, $state, appStates) {
        $scope.userData = {
            role: "ROLE_USER"
        };

        $scope.onRegisterFormSubmitted = function () {
            authService.register($scope.userData.username, $scope.userData.password, $scope.userData.role).then(function () {
                $state.go(appStates.login.name);
            });
        };
    }]);