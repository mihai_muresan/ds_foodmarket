foodMarket.controller('MainController', ['$scope', 'authService', '$state', 'appStates',
    function ($scope, authService, $state, appStates) {
        'use strict';

        $scope.onLogoutBtnClicked = function () {
            authService.logout().then(function () {
                $state.go(appStates.login.name);
            });
        }
    }]);