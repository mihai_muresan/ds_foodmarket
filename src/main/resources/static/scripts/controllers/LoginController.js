foodMarket.controller('LoginController', ['$scope', 'authService', '$state', 'appStates',
    function ($scope, authService, $state, appStates) {

        $scope.userData = {};

        $scope.onLoginFormSubmitted = function () {
            authService.login($scope.userData.username, $scope.userData.password).then(function (user) {
                if (user.role === "ROLE_ADMIN") {
                    $state.go(appStates.admin.home.name);
                } else {
                    $state.go(appStates.home.name);
                }
            });
        };
    }]);