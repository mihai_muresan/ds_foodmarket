foodMarket.factory('cartService', [function () {
    'use strict';

    var cart = [];

    function add(product) {
        product.isInCart = true;
        cart.push({
            product: product,
            quantity: 1
        });
    }

    function remove(product) {
        product.isInCart = false;

        for (var i = 0; i < cart.length; i++) {
            var cartItem = cart[i];

            if (product.id === cartItem.product.id) {
                cart.splice(i, 1);
                return false;
            }
        }
    }

    function getAll() {
        return cart;
    }

    return {
        add: add,
        remove: remove,
        getAll: getAll
    };
}]);