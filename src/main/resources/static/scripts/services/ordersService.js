foodMarket.factory('ordersService', ['$resource', 'apiUrls', function ($resource, apiUrls) {
    'use strict';

    var data = $resource(apiUrls.admin.order, {orderId: '@id'}, {
        update: {
            method: 'PUT'
        }
    });

    return data;
}]);