foodMarket.factory('productsService', ['$resource', 'apiUrls', function ($resource, apiUrls) {
    'use strict';

    var data = $resource(apiUrls.admin.product, {productId: '@id'}, {
        update: {
            method: 'PUT'
        }
    });

    return data;
}]);