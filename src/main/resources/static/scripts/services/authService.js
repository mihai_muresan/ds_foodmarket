foodMarket.factory('authService', ['$resource', 'apiUrls', '$http', function ($resource, apiUrls, $http) {
    'use strict';

    var userResource = $resource(apiUrls.user),
        storageUser = localStorage.getItem("food-market-user"),
        currentUser = {};

    if (storageUser) {
        currentUser = JSON.parse(storageUser) || {};
    }

    function register(username, password, role) {
        return userResource.save({username: username, password: password, role: role}, function (user) {

        }).$promise;
    }

    function login(username, password) {
        return userResource.get({username: username, password: password}, function (user) {
            currentUser = user;
            localStorage.setItem("food-market-user", JSON.stringify(currentUser));
        }).$promise;
    }

    function logout() {
        return $http.post(apiUrls.logout).then(function () {
            localStorage.removeItem("food-market-user");
            currentUser = {};
        });
    }

    function getCurrentUser() {
        return currentUser;
    }

    function isLoggedIn() {
        return currentUser.id !== undefined;
    }

    function isAdmin() {
        return currentUser.role === "ROLE_ADMIN";
    }

    return {
        register: register,
        login: login,
        getCurrentUser: getCurrentUser,
        isLoggedIn: isLoggedIn,
        isAdmin: isAdmin,
        logout: logout
    };
}]);