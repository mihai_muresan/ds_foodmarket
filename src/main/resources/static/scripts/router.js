foodMarket.config(['$stateProvider', '$urlRouterProvider', 'appStatesProvider', 'appConfigProvider',
    function ($stateProvider, $urlRouterProvider, appStatesProvider, appConfigProvider) {
        'use strict';

        var appConfig = appConfigProvider.$get(),
            appStates = appStatesProvider.$get();

        $urlRouterProvider.otherwise("/login");

        $stateProvider
            .state('main', {
                abstract: true,
                templateUrl: appConfig.viewsFolderPath + 'layout.main.html',
                controller: 'MainController'
            })
            .state(appStates.login.name, {
                templateUrl: appConfig.viewsFolderPath + 'login.html',
                url: appStates.login.url,
                controller: 'LoginController'
            })
            .state(appStates.register.name, {
                templateUrl: appConfig.viewsFolderPath + 'register.html',
                url: appStates.register.url,
                controller: 'RegisterController'
            })
            .state(appStates.home.name, {
                templateUrl: appConfig.viewsFolderPath + 'home.html',
                url: appStates.home.url,
                controller: 'HomeController',
                data: {
                    role: "ROLE_USER"
                }
            })
            .state(appStates.cart.name, {
                templateUrl: appConfig.viewsFolderPath + 'cart.html',
                url: appStates.cart.url,
                controller: 'CartController',
                data: {
                    role: "ROLE_USER"
                }
            })
            .state(appStates.orderSent.name, {
                templateUrl: appConfig.viewsFolderPath + 'order-sent.html',
                url: appStates.orderSent.url,
                data: {
                    role: "ROLE_USER"
                }
            })
            .state(appStates.admin.home.name, {
                templateUrl: appConfig.viewsFolderPath + 'admin/home.html',
                url: appStates.admin.home.url,
                controller: 'AdminHomeController',
                data: {
                    role: "ROLE_ADMIN"
                }
            })
            .state(appStates.admin.listProducts.name, {
                templateUrl: appConfig.viewsFolderPath + 'admin/products.html',
                url: appStates.admin.listProducts.url,
                controller: 'AdminProductsController',
                data: {
                    role: "ROLE_ADMIN"
                }
            })
            .state(appStates.admin.listOrders.name, {
                templateUrl: appConfig.viewsFolderPath + 'admin/orders.html',
                url: appStates.admin.listOrders.url,
                controller: 'AdminOrdersController',
                data: {
                    role: "ROLE_ADMIN"
                }
            })
            .state(appStates.admin.createProduct.name, {
                templateUrl: appConfig.viewsFolderPath + 'admin/product.html',
                url: appStates.admin.createProduct.url,
                controller: 'AdminProductController',
                data: {
                    role: "ROLE_ADMIN"
                }
            })
    }]);